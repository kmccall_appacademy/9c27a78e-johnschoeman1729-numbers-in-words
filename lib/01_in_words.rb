require 'byebug'
class Fixnum
  @@nums_to_words = {
    1 => "one",
    2 => "two",
    3 => "three",
    4 => "four",
    5 => "five",
    6 => "six",
    7 => "seven",
    8 => "eight",
    9 => "nine",
    10 => "ten",
    11 => "eleven",
    12 => "twelve",
    13 => "thirteen",
    14 => "fourteen",
    15 => "fifteen",
    16 => "sixteen",
    17 => "seventeen",
    18 => "eighteen",
    19 => "nineteen",
    20 => "twenty",
    30 => "thirty",
    40 => "forty",
    50 => "fifty",
    60 => "sixty",
    70 => "seventy",
    80 => "eighty",
    90 => "ninety"
  }

  def in_words
    return "zero" if self == 0

    magnitudes = ["", 'thousand', 'million', 'billion', 'trillion']

    num_by_period(self).reverse.reduce("") do |acc, group|
      if group == 0
        magnitudes.shift
        acc
      else
        "#{hundreds_in_words(group)} #{magnitudes.shift} " + acc
      end
    end.split.join(" ")
  end

  private

  def num_by_period(num)
    res = []
    while num > 0
      res.unshift(num % 1000)
      num /= 1000
    end
    res
  end

  def tens_in_words(num)
    return @@nums_to_words[num] if num < 21
    "#{@@nums_to_words[(num / 10) * 10]} #{@@nums_to_words[num % 10]}"
  end

  def hundreds_in_words(num)
    if num.to_s.length <= 2
      tens_in_words(num).to_s
    else
      "#{@@nums_to_words[num / 100]} hundred #{tens_in_words(num % 100)}"
    end
  end

end
